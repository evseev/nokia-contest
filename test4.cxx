// test4.cxx
// count positive bits, construct max/min value

#include <iostream>
#include <bitset>
#include <cstdint>

using namespace std;

const int NBITS = sizeof(uint32_t) * 8;

int count_positive_bits(uint32_t val)
{
	int n = 0;
	for (int i = 0; i < NBITS; i++) {
		n += (val & 1);
		val >>= 1;
	}
	return n;
}

uint32_t highest_val(int bits)
{
	uint32_t n = 0;
	for (int i = 1; i <= bits; i++)
		n |= (1 << (NBITS - i));
	return n;
}

uint32_t lowest_val(int bits)
{
	uint32_t n = 0;
	for (int i = 0; i < bits; i++)
		n |= (1 << i);
	return n;
}

int main(void)
{
	uint32_t val;
	while(cin >> val) {
		int n = count_positive_bits(val);
		uint32_t hi = highest_val(n);
		uint32_t lo = lowest_val(n);
		cout << "value = "   << dec << val << " (0b" << bitset<32>(val) << "), "
		     << "highest = " << dec << hi  << " (0b" << bitset<32>(hi)  << "), "
		     << "lowest = "  << dec << lo  << " (0b" << bitset<32>(lo)  << ")"
		     << endl;
	}
	return 0;
}

// END //

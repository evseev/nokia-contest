// test5.cxx
// find the largest path in binary tree

#include <iostream>
#include <algorithm>  /* max() */

using namespace std;

struct TreeNode {
	TreeNode *leftChild;
	TreeNode *rightChild;
};

int find_max_depth(TreeNode *top);

int find_max_depth(TreeNode *top)
{
	if (!top) return 0;
	int a = find_max_depth(top->leftChild);
	int b = find_max_depth(top->rightChild);
	return 1 + max(a,b);
}

// Testing-related stuff...

TreeNode *node_create()
{
	TreeNode *p = new TreeNode;
	p->leftChild = p->rightChild = 0;
	return p;
}

TreeNode *node_add(TreeNode *top, const string &route)
{
	for (const char *p = route.c_str(); *p; p++) {
		switch(*p) {
		case 'l':
		case 'L':
			if (!top->leftChild)
				top->leftChild = node_create();
			top = top->leftChild;
			break;
		case 'r':
		case 'R':
			if (!top->rightChild)
				top->rightChild = node_create();
			top = top->rightChild;
			break;
		}
	}
	return top;
}

int main(void)
{
	TreeNode *top = node_create();

	string s;
	while (cin >> s)
		node_add(top, s);

	cout << find_max_depth(top) << endl;

	return 0;
}

// END //

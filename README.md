# README for NokiaContest

### Requirements:

* gcc-c++
* gmake

### Contents:

* **test1** -- print prime numbers up to given value
* **test2** -- print count of words grouping by word length
* **test3** -- delete every fifth item from the linked list
* **test4** -- count positive bits, construct highest/lowest possible value
* **test5** -- find the longest path in binary tree

### How to build and run:

* **make** -- generates all binaries
* **make test** -- executes tests for all binaries
* **make clean** -- removes binaries
* **make dist** -- creates NokiaContest.tgz

### Notes:

* input is readed from STDIN, not from command line
* testing input is stored in xx.in file
* testing output is compared (and should match) with xx.out file

// test3.cxx
// Delete every fifth item from the linked list

#include <iostream>
#include <string>

using namespace std;

typedef string SomeDataType;

typedef struct List {
	struct List * next;
	SomeDataType payload;
} List_t;

List_t *list_append(List_t *head, string s)
{
	if (!head) {
		head = new List_t;
	} else {
		while (head->next) head = head->next;
		head = head->next = new List_t;
	}
	head->next = 0;
	head->payload = s;
	return head;
}

void list_delete_every_nth(List_t *head, int nth)
{
	List_t *cur = head, *prev = 0;
	int pos = 0;
	while (cur) {
		if ((++pos % nth) == 0) {
			List_t *x = cur;
			prev->next = cur = cur->next;
			delete x;
		} else {
			prev = cur;
			cur = cur->next;
		}
	}
}

int main(void)
{
	List_t *head = 0;

	// Fill initial data from standard input..
	string s;
	while (cin >> s) {
		List_t *last = list_append(head, s);
		if (!head) head = last;
	}

	list_delete_every_nth(head, 5);

	// Print result..
	for (List_t *p = head; p; p = p->next)
		cout << p->payload << endl;

	return 0;
}

// END //

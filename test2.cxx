// test2.cxx
// Stat length of words

#include <iostream>
#include <map>
#include <cctype>  /* isspace */

using namespace std;

int main(void)
{
	char c;
	map<int,int> wordlen;  //..sorted for better output
	int len = 0;

	while(cin.get(c)) {
		if (!isspace(c)) {
			//clog << "DEBUG: char = " << c << ", len = " << len << endl;
			len++;
			continue;
		}
		//clog << "DEBUG: space reached, len = " << len << endl;
		if (!len)
			continue;

		// End of word reached - update stats..
		if (wordlen.count(len) == 0) {
			wordlen[len] = 1;
		} else {
			wordlen[len]++;
		}
		//clog << "DEBUG: len = " << len << ", count = " << wordlen[len] << endl;
		len = 0;
	}
	//clog << "DEBUG: loop finished" << endl;

	// Print results..
	for (auto it = wordlen.cbegin(); it != wordlen.cend(); it++)
		cout << it->first << " => " << it->second << endl;

	return 0;
}

// END //

#!/usr/bin/gmake -f

ALL_PROGS = test1 test2 test3 test4 test5

all: $(ALL_PROGS)

clean:
	/bin/rm $(ALL_PROGS)

dist:
	tar czf NokiaContest.tgz *.cxx *.in *.out Makefile Runtest README* .gitignore

test: all
	@for p in $(ALL_PROGS); do ./Runtest "$$p"; done

%: %.cxx
	g++ $< -o $@

## END ##

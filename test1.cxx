// test1.cxx
// print prime numbers

#include <iostream>
#include <vector>

using namespace std;

typedef vector<unsigned> primebuf;

bool isprime(const primebuf& primes, unsigned val)
{
	if (val > 10 && ((val % 10) == 5))
		return false;

	for (auto it = primes.cbegin(); it != primes.cend(); it++) {
		unsigned j = *it;
		if (((j * j) - 1) > val)
			return true;
		if ((val % j) == 0)
			return false;
	}

	return true;
}

int main(void)
{
	unsigned max;
	if (!(cin >> max))
		return 1;

	vector<unsigned> primes;
	primes.push_back(2);

	// Calculate...
	for (int i = 3; i <= max; i += 2) {
		if (isprime(primes, i))
			primes.push_back(i);
	}

	// Print result...
	for (auto it = primes.cbegin(); it != primes.cend(); it++) {
		unsigned i = *it;
		if (i > max) break;
		cout << i << endl;
	}

	return 0;
}

// END //
